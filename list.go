package main

import (
	"database/sql"
	"fmt"
	"os"
)

func usage_list() {
	fmt.Println(os.Args[0], " list [tag]")
}

func list(c *sql.DB) {
	var count int
	var err error
	var data *sql.Rows
	if len(os.Args) > 2 {
		data, err = c.Query("SELECT id, note, tag FROM notes WHERE tag=? ORDER BY id ASC", os.Args[2])
		if err != nil {
			eprintln(err.Error())
			return
		}
	} else {
		data, err = c.Query("SELECT id, note, tag FROM notes ORDER BY id ASC")
		if err != nil {
			eprintln(err.Error())
			return
		}
	}

	for data.Next() {
		var note, tag string
		var id int
		err = data.Scan(&id, &note, &tag)
		if err != nil {
			continue
		}
		count++
		fmt.Printf("%d. %s #%s\n", id, note, tag)
	}

	fmt.Printf("\n%d notes returned.\n", count)
}

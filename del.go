package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func usage_del() {
	fmt.Println(os.Args[0], " del id")
}

func del(c *sql.DB) {
	if len(os.Args) < 3 {
		usage_del()
		return
	}
	var note, data string
	needs_input := true

	id, err := strconv.Atoi(os.Args[2])
	if err != nil {
		eprintln(err)
		return
	}

	row := c.QueryRow("SELECT note FROM notes WHERE id=? LIMIT 1", id)
	err = row.Scan(&note)
	if err != nil {
		eprintln(err)
		return
	}

	for needs_input {
		print("Are you sure you want to remove the note \"", note, "\"? [Y/n] ")
		_, err = fmt.Scanln(&data)
		if err != nil {
			if err.Error() == "EOF" {
				data = "n"
				print("\n")
			} else if err.Error() != "unexpected newline" {
				eprintln(err.Error())
			}
		}
		switch strings.ToLower(data) {
		case "":
			fallthrough
		case "y":
			fallthrough
		case "yes":
			needs_input = false
		case "n":
			fallthrough
		case "no":
			println("The note was not removed")
			return
		}
	}

	_, err = c.Exec("DELETE FROM notes WHERE id=?", id)
	if err != nil {
		eprintln(err)
		return
	}

	// TODO: Check rows affected here

	_, err = c.Exec("UPDATE notes SET id = id - 1 WHERE id > ?", id)
	if err != nil {
		eprintln(err)
		return
	}

	fmt.Println("Note removed")
}

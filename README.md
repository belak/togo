# togo
Also known as to-go. This is a rewrite of my original command line app todo, written in go with a few improvements.

# Installation
Required packages:

* go
* go-sqlite3 (https://github.com/mattn/go-sqlite3)

package main

import (
	"database/sql"
	"fmt"
	"os"
	"strings"
)

func usage_add() {
	fmt.Println(os.Args[0], " add [tag] note")
}

func add(c *sql.DB) {
	argc := len(os.Args)
	if argc < 3 {
		usage_add()
		return
	}
	var err error
	//var data sql.Result
	stmt, err := c.Prepare("INSERT INTO notes VALUES (NULL, ?, ?)")
	if err != nil {
		eprintln(err.Error())
		return
	}

	if os.Args[2][:1] == "#" && argc > 3 {
		_, err = stmt.Exec(strings.Join(os.Args[3:], " "), os.Args[2][1:])
	} else {
		_, err = stmt.Exec(strings.Join(os.Args[2:], " "), new(sql.NullString))
	}
	if err != nil {
		eprintln(err.Error())
		return
	}

	// TODO: Actually use the ReturnedRows()

	stmt.Close()
}

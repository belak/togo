package main

import (
	"bytes"
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"os"
	"runtime"
)

func version() (ver string) {
	return "0.0.1"
}

func usage() {
	fmt.Println(os.Args[0], version())
	fmt.Println(os.Args[0], "[module]", "[options]")
	fmt.Println()
	usage_add()
	usage_list()
	usage_del()
}

func main() {
	argc := len(os.Args)
	home := os.Getenv("HOME")
	if len(home) == 0 {
		eprintln("$HOME not set")
		return
	}

	var buffer bytes.Buffer
	buffer.WriteString(home)
	buffer.WriteString("/.config/belak/")

	err := os.MkdirAll(buffer.String(), 755)
	if err != nil {
		eprintln(err.Error())
		return
	}

	buffer.WriteString("togo.db")

	_, err = os.Stat(buffer.String())
	first := os.IsNotExist(err)

	c, err := sql.Open("sqlite3", buffer.String())
	if err != nil {
		eprintln(err.Error())
		return
	}
	defer c.Close()

	if first {
		// TODO: Check the AffectedRows()
		_, err = c.Exec("CREATE TABLE notes (id INTEGER PRIMARY KEY, note TEXT NOT NULL, tag TEXT)")
		if err != nil {
			eprintln(err.Error())
			return
		}

		_, err = c.Exec("CREATE TABLE options (id INTEGER PRIMARY KEY, name TEXT NOT NULL, value TEXT NOT NULL)")
		if err != nil {
			eprintln(err.Error())
			return
		}
	}

	if argc > 1 {
		switch os.Args[1] {
		case "about":
			fmt.Printf("notes\nbuilt with %s\n", runtime.Version())
		case "l", "list":
			list(c)
		case "a", "add":
			add(c)
		case "d", "del":
			del(c)
		default:
			usage()
		}
	} else {
		usage()
	}
}

package main

import "fmt"
import "os"

func eprintf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	fmt.Print("\n")
}

func eprintln(args ...interface{}) {
	fmt.Fprintln(os.Stderr, args...)
}
